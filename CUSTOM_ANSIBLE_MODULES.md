# Custom Ansible Modules

## Why a custom module?
Ansible has a bajillion vetted and useful modules.  If you can't find a 
module that does what you need, you can use the Ansible shell, command, 
script, or raw plugins to do what you need.  So why take the time to 
write something custom?

Playbooks should be easy to follow, be declarative, be idempotent, 
and be deterministic. 
* "This makes sense, even though I don't know ansible well, I can read it," 
is goal number one. 
* Goal two - Ansible leans heavily on declarative statements, so
playbooks should state how the end result will look, not on the commands
used to get it there.  Instead of "start firewalld with this command,"
an Ansible playbook will say, "once this playbook is finished, firewalld
should be running."  Someone writing the playbook doesn't necessarily 
need to know how to start it, they simply need to tell Ansible that it 
should be started when the playbook finishes.
* Playbooks shouldn't redo what has been done already unless forced to.
If running the command will not change the system then do not run it 
again - Goal three.
* Even if the play were run again the result should remain the same, i.e.
 it should be deterministic, with is the fourth goal. 

When a playbook strays away from these desires, writing custom modules 
helps right the ship. [Take a look at this useful article about the subject](https://opensource.com/article/19/3/developing-ansible-modules)


## Basic Ansible Module Structure
(grabbed from [http://devopstechie.com/create-custom-ansible-module/])

The Ansible Module Format
* Shebang: #!/usr/bin/python
* License agreement: the Ansible free software agreement since this is 
an open source product
* Documentation: properly formatted yaml documentation (and 
documentation can reference other modules if needed)
* Examples: use case for this module
* Returns: you need to include a list of what is returned by the module
* Functions: various functions your module needs, like changing a port 
on an ELB
* Main: testing will be a nightmare if you don’t use a Main

### Documentation
Fully document the options with this YAML formatting
```yaml
Documentation = '''
module: our_module

short_description: A module to do things

version_added: "2.8"

description:
    - "Does this thing"

options:
    theFirstThingThisCanDo:
         description: 
             - use this to do stuff
         required: true
    theSecondThingThisCanDo:
         description:
             - use this to do other stuff
         required: false     
'''
```

### Examples
Provide examples of how the syntax will look and operate in a playbook
```yaml
EXAMPLES = '''
# Do this thing
- name: Do it
  our_module:
    theFirstThingThisCanDo: 'be awesome'

# Do the thing AND the other thing
- name: Do both things
  our_module:
    theFirstThingThisCanDo: 'maintain awesome'
    theSecondThingThisCanDo:  'improve awesomeness'
'''
```

### Return
What will the module give back as output?
```yaml
RETURN = '''
message:
    description: a visual representation of awesomeness
'''
```

### Functions
This is where the meat of the module goes.  Foremost, the AnsibleModule
is needed, so import it.  This module contains the methods and functions
the module will need to speak with Ansible.  Ansible requires consistent
input and output amongst it's modules.  AnsibleModule will make it 
easier to adhere to that.
```python
from ansible.module_utils.basic import AnsibleModule
```

The arguments necessary for the module to run, whether they are located 
in the documentation or not, are easily passed to the module using 
`argument_spec`. It is a dict of dicts with the arguments.  It resembles
 argparse quite a bit. 
 ([All of the various attributes can be found here](https://docs.ansible.com/ansible/latest/dev_guide/developing_program_flow_modules.html#argument-spec).)  
An example:
```python
module_args = dict(
    theFirstThingThisCanDo=dict(type='str', required=True),
    theSecondThingThisCanDo=dict(type='str', required=False, default='Be more awesome'),
    theThirdThingThisCanDo=dict(type='bool', required=False, default=True)
)

module = AnsibleModule(
    argument_spec=module_args
)
``` 
In the current example, the next thing is the `main` function.  This is
the entrypoint to the module.

```python
def run_module():
    if module.params['theFirstThingThisCanDo']:
        make_the_awesome(module.params['theFirstThingThisCanDo'])
    elif module.params['theFirstThingThisCanDo'] and module.params['theSecondThingThisCanDo']:
        make_all_the_awesome(module.params['theSecondThingThisCanDo'])
    elif module.params['theThirdThingThisCanDo']:
        module.exit_json(changed=False, message='How did you do that?')
    else:
        module.fail_json(msg='It is not awesome at all')
```    
Next, create the worker functions.  The functions either need to return 
the results as a json block or call `module.exit_json` or `module.fail_json`.
```python
def make_the_awesome(aweyeah):
    if aweyeah == 'the awesome'
        status_one = false
        message_one = aweyeah + aweyeah + aweyeah
    else:
        try:
            notyet = aweyeah.upper() + 'is lacking in awesome.\n'
            nowitis = notyet + 'we should make it:' + aweyeah 
            status_one = True
            message_one = nowitis
        except Exception as e:
            module.fail_json(msg='The fail happened because: {0}'.format(e))
        
    results = dict(
        changed=status_one,
        message=message_one
    )
    module.exit_json(**results) 
```
If the person running the playbook put in the correct phrase, the result
will be an unchanged string, so that status change is False.  It the
person put in a different phrase, it will change the phrase and return 
with a status change of True.  These values are passed to the `exit_json` 
function that is then interpreted by the ansible.module_utils.basic 
module and provided to Ansible.

## Further
_Not sure what to call this._  

* From what I've been able to discern, the two most important things that 
modules really need to know are, "did it change?" and "what should I say
about it?"  If you can present it those elements in a block that matches
the formatting it expects to see, you'll be able to make a working 
ansible module.
* It doesn't have to be Python but Python is HIGHLY recommended as there
are the python modules there to make integration much easier.  
* These should be added to the `library` directory of the ansible project
root or in a `library` folder within a role.  Ansible will automatically 
pick them up.
* I have done anything with checkmode yet, that will be coming soon.

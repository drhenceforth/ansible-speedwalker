The Rocketchat module outputs to a rocket chat, pretty duh.

* You need a rocket chat incoming webhook integration needs to exist within rocketchat.
* simple rocket.chat ansible module syntax looks like this:
  ```yaml
  tasks:
    - name: some other task
      an_ansible_module:
        do: ansible things
      register: module_output
    
    - name: Rocket.chat status update message
      rocketchat:
        token: A_lOng_stRInG-giVEN=By+Rocket.chat
        domain: chat.yourdomain.org
        msg: "You can put the message from the registered variable dictionary here {{ message }}."

#!/usr/bin/python
# rwinne
#

import psycopg2
import re
import sys
import logging
import base64 as b6
import json
import yaml
from psycopg2.extras import RealDictCursor

# for when the db is setup
#opdwd = b6.b64decode("ENCODED_PASSWORD")

logger = logging.getLogger(__name__)

def connect_hosts(*args):
    if not args:
        logger.info('No hosts passed')
        pass
    elif args and len(args) == 1:
        args = args[0].split()
        args = [i.upper().rstrip(',') for i in args]
        host_input = args
        logger.info('Running against these hosts: {0}'.format(host_input))
    elif re.search('/@', args[0]):
        host_input_file = re.sub('@', '', args[0])
        if host_input_file == '/etc/ansible/hosts':
            host_input = None
        with open(host_input_file, 'r') as host_list:
            host_input = [line.upper().rstrip('\n') for line in host_list]
        logger.info('Running against these hosts: {0}'.format(host_input))
    elif args:
        host_input = [i.upper() for i in args]
        logger.info('Running against these hosts: {0}'.format(host_input))

    sou = []
##TODO configure sql, postgres, query to take the requested hosts and respond with host specific info
#    try:
#        with psycopg2.connect(dbname="", user="", host="", password=opdwd) as soucon:
#            with soucon.cursor(cursor_factory=RealDictCursor) as soucur:
#                if host_input:
#                    for i in host_input:
#                        soucur.execute("select id,connection,port from connections where id = %s;", (i,))
#                        sou.append(soucur.fetchone())
#               else:
#                    soucur.execute("select id,connection,port from connections;")
#                    sou = soucur.fetchall()
#    except Exception as e:
#        logger.info('Unable to connect to the database because {0}'.format(e))
#    sou = [i for i in sou if i is not None]
    return sou

##TODO figure out how to get the the hosts list.  maybe also give options. if I get everything else working, I could make it wonderfully variable
def write_inventory_ini(host_out_file, playbook, input_inv_host_list):
    """
    Takes a list of dictionaries, writes them to an ansible hosts file
    :return: Writes the hosts file - does not return any data, only a 
    boolean True for success.
    """

    try:
        with open(host_out_file, "w") as filecontent:
            filecontent.write('[' + playbook + ']\n')
            logger.info('Writing playbook\'s inventory to: {0}'.format(host_out_file))
            for i in input_inv_host_list:
                for k, v in i.iteritems():
                    filecontent.write('{0} ansible_host={1} ansible_ssh_private_key_file=/etc/ansible/.ssh/{0}.pem ansible_user=ansible-admin\n'.format(i['id'], i['connection']))

    except Exception as e:
        logger.debug("because {0}".format(e))

    return True

if __name__ == '__main__':
    host_list_output = connect_hosts('infra1','infra2')
    write_inventory_ini('/tmp/ini4hosts','playbookie_goodness',host_list_output)

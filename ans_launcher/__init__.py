import argparse
import yaml
import os
import ans_launcher.ans_inv_generate as dyninv
import logging.config
import subprocess
import datetime as dt

fh = logging.FileHandler('/var/log/ansible/ans_launcher_log')
formatter = logging.Formatter('%(asctime)s - %(levelname)s | %(message)s', datefmt='%Y-%m-%d %H:%M')
fh.setFormatter(formatter)
logger = logging.getLogger(__name__)
logger.addHandler(fh)
logger.setLevel(logging.INFO)

# os.environ['ANSIBLE_FORCE_COLOR'] = '1'

class Ans:
    """
    The main class for the ansible stuff I'm putting together.
    from here
    """

    def __init__(self):
        self.name = self
        logger.info('=' * 80)

    def theconf(self, **kwargs):
        asc = AnsSystemsConfig()
        try:
            ans = ams.merged(kwargs, asc.ans_conf)
        except AttributeError:
            ans = ams.args.__dict__
        if kwargs:
            ans = asc.merged(ans, asc.args.__dict__)
        return ans

    def archive_dir(self, basepath, playbook_name):
        playbook_name = os.path.basename(os.path.normpath(playbook_name))
        timestamp = dt.datetime.now().strftime('%Y%m%d_%H%M%S_%f')
        basepath = os.path.normpath(basepath)
        resultpath = '{0}/{1}_{2}'.format(basepath, playbook_name, timestamp)
        logger.info('The working directory for this run is: ' + resultpath)
        os.mkdir(resultpath)
        return resultpath

    ansible_work_directory = '/opt/ansible/ansible-playbooks'
    # ans_launcher_log_file = '/var/log/ansible/ans_launcher_log'


class AnsSystemsConfig:
    def __init__(self):
        self.name = self
        ##TODO figure out how to pass dictionaries for -e and -s from the commandline and kwargs
        parser = argparse.ArgumentParser(description='Create a list of connection argument')
        parser.add_argument('-p', '--playbook', help='Path to the playbook.')
        parser.add_argument('-s', '--sudo', action='store_true', help='Become sudo on the remote host.')
        parser.add_argument('--vault_id', help='Location of the vault-id file if different from the default.')
        parser.add_argument('-e', '--extra_args', nargs='*', help='Optional extra arguments to pass to the playbook.')
        parser.add_argument('-v', '--verbose', action='count', default=0,
                            help='Verbose output - add multiple to increase verbosity')
        parser.add_argument('-u', '--user', help='Specify a different user.')
        parser.add_argument('-k', '--ask_pass', action='store_true',
                            help='Prompt for the password of the user trying to connect.')
        parser.add_argument('-i', '--inventory', '--inventory_file',
                            help='alternate inventory.  Include the "@" before the file name to denote a file is being used')
        parser.add_argument('-K', '--ask_sudo_pass', action='store_true', help='Ask for the sudo password')
        parser.add_argument('-c', '--callback', help='Specify a non-default callback script')
        parser.add_argument('-S', '--ssh_args', nargs='*', help='Common extra args for all ssh CLI tools.')
        parser.add_argument('-f', '--config_file',
                            help='Specify a different YAML file than the default configuration options file.  File options are overridden by CLI arguments')
        parser.add_argument('-I', '--ignore_config_file', action='store_true',
                            help='Ignores configuration files entirely.')
        self.args = parser.parse_args()

        if self.args.ignore_config_file:
            default_conf = None
        elif self.args.config_file:
            default_conf = args.config_file
        elif os.path.exists('../vars/sys_ansible_default_config'):
            default_conf = '../vars/sys_ansible_default_config'
        elif os.path.exists('vars/sys_ansible_default_config'):
            default_conf = 'vars/sys_ansible_default_config'
        elif os.path.exists('opt/ansible/ansible_playbooks/vars'):
            default_conf = '/opt/ansible/ansible_playbooks/vars'
        else:
            default_conf = None

        if default_conf:
            with open(default_conf, 'r') as yamlfile:
                try:
                    self.ans_conf = yaml.safe_load(yamlfile)
                except yaml.YAMLError as e:
                    logger.debug('YAMLError: {0}'.format(e))

    def merged(self, dict1, dict2):
        dict1.update({k: v for k, v in dict2.iteritems() if v})
        return dict1


def the_cmd_list_to_send(conf, workdir=Ans.ansible_work_directory):
    """
    takes all the values in the arguments dictionary and uses them to build the subprocess command
    string.
    the valid kwargs are
    :return: the values of the argument dictionary in the form of a list
    """
    # print(conf['inventory'])
    try:
        if conf['playbook'] is None or conf['playbook'] == '':
            the_cmd_list = ['The playbook is specified as "None" - please add a playbook']
            return the_cmd_list
        else:
            the_cmd_list = ['ansible-playbook', conf['playbook']]
    except KeyError as e:
        logger.debug('Required key missing: {0}'.format(e))
        the_cmd_list = ['the return data of this method is in error state']
        return the_cmd_list

    cmd_dict = {}

    def dict_args(args_dict, cli_arg):
        dict_listout = [cli_arg]
        if isinstance(args_dict, dict):
            for k, v in args_dict.iteritems():
                dict_listout.append(k + '=' + v)
            dict_listout[1] = '"' + dict_listout[1]
            dict_len = len(dict_listout) - 1
            dict_listout[dict_len] = dict_listout[dict_len] + '"'
            return dict_listout
        else:
            del cmd_dict['args_dict']

    def toggle_args(args_toggle, cli_arg):
        if args_toggle is True:
            toggle_listout = [cli_arg]
        return toggle_listout

    def str_args(args_string, cli_arg):
        if args_string:
            args_listout = [cli_arg, args_string]
        return args_listout

    for k, v in conf.iteritems():
        try:
            if k == 'ask_pass':
                cmd_dict[k] = toggle_args(conf[k], '-k')
            if k == 'ask_sudo_pass':
                cmd_dict[k] = toggle_args(conf[k], '-K')
            if k == 'sudo':
                cmd_dict[k] = toggle_args(conf[k], '--become')
            if k == 'vault_id':
                cmd_dict[k] = str_args(conf[k], '--vault-id')
            if k == 'user':
                cmd_dict[k] = str_args(conf[k], '-u')
            if k == 'extra_args':
                cmd_dict[k] = dict_args(conf[k], '-e')
            if k == 'ssh_args':
                cmd_dict[k] = dict_args(conf[k], '--ssh-common-args')
            if k == 'verbose':
                if v == 0:
                    pass
                elif v <= 5:
                    cmd_dict[k] = ['-' + 'v' * conf[k]]
            if k == 'callback':
                cmd_dict[k] = 'ANSIBLE_STDOUT_CALLBACK=' + conf[k]
            if k == 'inventory':
                ansible_hosts_dir = Ans().archive_dir(workdir, conf['playbook'])
                # print(ansible_hosts_dir)
                ansible_hosts_file = ansible_hosts_dir + '/ansible_hosts'
                # print('dictvalue = ' + conf[k])
                dicq = dyninv.connect_query(conf[k])
                # print('dictcontent = {0}'.format(dicq))
                dyninv.write_inventory_ini(ansible_hosts_file, conf['playbook'], dicq)
                cmd_dict['k'] = ['-i', ansible_hosts_file]

        except KeyError:
            logger.error('pass KeyError - no: ' + k)
        except UnboundLocalError:
            logger.error('pass UnboundLocalError - no: ' + k)

    # the_cmd = ['ansible-playbook'] + [val for sublist in cmd_dict.values() for val in sublist]

    try:
        for sub_elements in cmd_dict.values():
            if isinstance(sub_elements, basestring):
                the_cmd_list.insert(0, sub_elements)
            else:
                for i in sub_elements:
                    the_cmd_list.append(i)

    except Exception as e:
        the_cmd_list = ['An error was caught by the catchall: {0}'.format(e)]
        logger.error(the_cmd_list)

    return the_cmd_list


##TODO I'm not sure which one I prefer yet.
def the_cmd(cmd_for_sublist):
    logger.info('The command list being passed to subprocess: {0}'.format(cmd_for_sublist))
    try:
        # the_cmd_subprocess = subprocess.Popen(cmd_for_sublist, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        # cmdout = the_cmd_subprocess.stdout.readlines()
        # cmdout = [i.strip() for i in cmdout if i != '\n']
        cmdout = subprocess.check_output(cmd_for_sublist, stderr=subprocess.STDOUT, universal_newlines=True)
        logger.info('The beginning of the subprocess.STDOUT')
        logger.info(cmdout)
        # for i in cmdout:
        #     loggier.info(i)
        logger.info('The end of the subprocess.STDOUT')

    except Exception as e:
        logger.debug('Method "the_cmd" failed due to: {0}'.format(e))

# Ansible Speedwalker launch package
A python package we can use to include Ansible Playbooks in Python scripts

## Basic Flow

`import ans_launcher`
* imports the package

`ans_launcher.Ans().theconf()`
* accepted options can be passed as a dictionary.

`ans_launcher.the_cmd_to_send(ans_launcher.Ans().theconf()`
* builds the command list for subprocess using the the configuration
grabbed from `theconf`

`ans_launcher.the_cmd(ans_launcher.the_cmd_to_send(ans_launcher.Ans().theconf())`

so this would work:
```python
import ans_launcher

theBaseConfiguration = ans_launcher.Ans().theconf(verbose=3)
theCommandWeWillRun = ans_launcher.the_cmd_list_to_send(theBaseConfiguration)
ans_launcher.the_cmd(theCommandWeWillRun)
```

It will put everything in an archive directory that is time stamped with
the playbook's name.

## Arguments

Arguments passed to `theconf` as kwargs  
 _have a higher priority than_  
Arguments added to the **commandline**  
_have a high priority than_  
Arguments included in `amsys_ansible_default_config` (which is a YAML file)

it will look at:
* files passed as the `config_file`, either in the kwargs of theconf or
in the cli as `-f` or `--config_file` 
* `../vars/amsys_ansible_default_config`
* `/vars/amsys_ansible_default_config`
* `/opt/ansible/ansible_playbooks/vars`

using `ignore_config_file` (or `-I` in the cli) will ignore the configuration file

## Hosts

When creating the playbook, if you want to pass a custom list of hosts,
use the name of the playbook in the ansible playbook in the `hosts` field.

The hosts can be passed in a file - as of right now, list the hosts
one per line, no punctuation.

The hosts can be passed a list.
* if you pass them as a list in the config YAML, put them in the 'inventory' line,
comma separated.
* if you pass them in the command line, encapsulate the chunk in quotes,
comma separate all the values within the quotes.

Or, you don't need a hosts file or list of hosts - the host list `/etc/ansible/hosts`
can be used for all or with any subgroup included in it.

## Notes
* the callback argument/option isn't working correctly yet.
* the output really needs some cleaning up, but it does log everything, just
kinda ugly right now because formatting it is a pain.
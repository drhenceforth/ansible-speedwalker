#!/usr/bin/python

## TODO create the yum version lock module. (Needed for a specific use case involved with version mismatching)

ANSIBLE_METADATA = {
    'metadata_version': '0.1',
    'status': ['preview'],
    'supported_by': 'drhenceforth'
}

DOCUMENTATION = '''
---
module: yum-verlock

short_description: This is my sample module

version_added: "2.7"

description:
    - "This is my longer description explaining my sample module"

options:
    name:
        description:
            - This is the rpm's name
        required: true
    unlock:
        description:
            - This unlocks the module in the version lock file
        required: false
    lock:
        description:
            - Locks the version of the rpm.
        required: false

author:
    - DrHenceforth (richardwinne@gmail.com)
'''
EXAMPLES = '''
# Unlocking an RPM
- name: unlock httpd
  yum-verlock:
    name: httpd
    unlock: true

# Locking an RPM
- name: lock htppd
  yum-verlock:
    name: httpd-2.4.6-93.el7.centos.x86_64.rpm
    lock: true

'''

RETURN = '''
original_message:
    description: The original name param that was passed in
    type: str
message:
    description: The output message that the sample module generates
'''

from ansible.module_utils.basic import AnsibleModule

def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        name=dict(type='str', required=True),
        new=dict(type='bool', required=False, default=False)
    )

    # seed the result dict in the object
    # we primarily care about changed and state
    # change is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False,
        original_message='',
        message=''
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    # if the user is working with this module in only check mode we do not
    # want to make any changes to the environment, just return the current
    # state with no modifications
    if module.check_mode:
        return result

    # manipulate or modify the state as needed (this is going to be the
    # part where your module will do what it needs to do)
    result['original_message'] = module.params['name']
    result['message'] = 'goodbye'

    # use whatever logic you need to determine whether or not this module
    # made any modifications to your target
    if module.params['new']:
        result['changed'] = True

    # during the execution of the module, if there is an exception or a
    # conditional state that effectively causes a failure, run
    # AnsibleModule.fail_json() to pass in the message and the result
    if module.params['name'] == 'fail me':
        module.fail_json(msg='You requested this to fail', **result)

    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)

def main():
    run_module()

if __name__ == '__main__':
    main()
#!/usr/bin/python

from __future__ import (absolute_import, division, print_function)
from ansible.plugins.callback import CallbackBase
import json
import datetime as dt

'''
Takes the output of a playbook and copies it to two files, one for the gathered facts and the 
other for the playbook output
'''

hnow = dt.datetime.now().strftime("%Y-%B-%d_%H%M%S")


class CallbackModule(CallbackBase):
    CALLBACK_VERSION = 2.0
    CALLBACK_NAME = "split-callback"
    CALLBACK_TYPE = "notification"

    def __init__(self):
        super(CallbackModule, self).__init__()

    def v2_runner_on_ok(self, result):
        host = result._host.get_name()
        taskname = result._task.get_name()
        # pbname = result._task.
        # log_dir = /var/log/ansible/runs
        if taskname == 'Gathering Facts':
            gfout_filename = '/tmp/gather_facts_{0}_{1}'.format(host, hnow)
            with open(gfout_filename, 'a') as outfile:
                json.dump(result._result, outfile, indent=2)
        else:
            pbout_filename = '/tmp/playbook_{0}_{1}'.format(host, hnow)
            with open(pbout_filename, 'a') as outfile:
                json.dump(result._result, outfile, indent=2)
    def v2_runner_on_failed(self, result, ignore_errors=False):


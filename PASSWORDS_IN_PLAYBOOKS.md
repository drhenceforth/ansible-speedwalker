# How to store passwords & secrets in Ansible playbooks

## Using the ansible-vault

Ansible has a feature called Ansible Vault, it is a way to store passwords
 and secrets securely.  Well written callback scripts should obfuscate the vaules passed
 as vault values.

For example, the pbis-installer playbook has a password in it for the
account used by pbis to add hosts to active directory.  We've named it `auth.pbis-install`.
The password for the account won't be visible anywhere in the process
or the playbook driving it. 

The vault-id file is a plaintext password.  This password is used to 
encrypt the corresponding vault file located in the playbooks.  (That is why
it is outside of the git project, as it is public.)  

We don't need new encrypt/decrypt password files in the auth directory - 
re-using ones for similar purposes should help reduce confusion and keep
 the store from becoming unwieldy.

The way I've been using it is as follows:
* create a yaml file with a key name and a value and give it a name you 
find descriptive.  Let's say the file that will be containing the secret
is named, `a_role_i_made/vars/the_passwd` and it contains:
`a_secure_password: khljhsdjhasfkljhawh`
* create a vault-id file, something like `/etc/ansible/auth/auth.secure_passwd`.
in that file, write a long, difficult password. Something like:
`df;jhas;ouyhaewrju796hwlhjh6565954+++767`.  Verify its permissions are
restrictive.
* if you aren't in a terminal, enter one.  Run the command `ansible-vault encrypt
a_role_i_made/vars/the_passwd --vault-id secure_passwd@/etc/ansible/auth/auth.secure_passwd`
(by adding the `secure_passwd@`, we're 'labelling' the encrypted file.  This
is not necessary but will start to be useful as we further increase the
complexity of our playbooks. The auth.secure_passwd file will still decrypt it)
* Open the `a_role_i_made/vars/the_passwd` file now.  It will look like this:
``` yaml
$ANSIBLE_VAULT;1.1;AES256;secure_passwd
38643434636132623561363234363137353661623966313337303165363933666439326533343339
3934633039653532613237326236393335656539383930340a336266613463393731623863633561
30646232353563393434323864356533559876333930343130333363333166623031303862626366
3434383663383231650a353738356630616133326661316463666435313063623263396437643335
63626234633764333639636430636135323565303936373931333535663633376635393030366538
6466653766386363393465663265343164656135306161396431
```
(If it wasn't labelled, the fourth semi-colon delimited field on the first 
line won't have a value.)
* Now add the file name in your playbook or role's base `vars_files` file:
```yaml
  vars_files:
     `a_role_i_made/vars/the_passwd`
```
* When calling the variable now, reference the password as `"{{ a_secure_password }}"`
* When running the playbook/role, use the argument `--vault-id` and include
the path to the `auth.secure_passwd` file you created.

When the playbook/role is run, it will use the file in `/etc/ansible/auth`
to decrypt the `the_passwd` file.

Use the argument `no_log: True` to keep the secret from being placed in
plaintext in the callback output.  (Well written modules that know they're asking
for a password should do this for you.)  Or you can declare `no_log: True`
on a whole play by including it at the top of the playbook before the tasks
are defined.

## Encrypted strings
Or you can encrypt strings in the playbooks and avoid using a vars file.  You would just add the 
encrypted string to the file itself.  Our password above would be encrypted like this:
`ansible-vault encrypt-string --vault-id a_secure_passwd 'khljhsdjhasfkljhawh' --name 'a_secure_passwd`
and the output would be what you dump into the playbook.  It would end up being:
```yaml
a_secure_passwd: !vault |
          $ANSIBLE_VAULT;1.2;AES256;a_secure_passwd
          37666464363865383663303739616439366531626130323564663639636262333531653164623261
          3964326465303836396638666164326564643739373662380a326134366231636538666535346532
          33356565373132636663612205303362613932303938373832313237343634626135386132643836
          3134336237343938610a343265333230303832366664316531306361666337646462356631643638
          33336261396666366330366230623465396332616135396335313636353965653739
```
 


## `ansible-vault` commands
* To encrypt a file with a label (`ansible-vault encrypt`):
`ansible-vault encrypt a_role_i_made/vars/the_passwd --vault-id secure_passwd@/etc/ansible/auth/auth.secure_passwd`

* To encrypt a string with a label (`ansible-vault encrypt_string`):
`ansible-vault encrypt_string --vault-id secure_passwd@/etc/ansible/auth/auth.secure_passwd 'khljhsdjhasfkljhawh' --name 'a_secure_passwd'`

* To view the file (`ansible-vault view`):  
`ansible-vault view a_role_i_made/vars/the_passwd --vault-id /etc/ansible/auth/auth.secure_passwd`

* To edit the file - opens in your default BASH editor - (`ansible-vault edit`):  
`ansible-vault edit a_role_i_made/vars/the_passwd
--vault-id /etc/ansible/auth/auth.secure_passwd`

* To rekey the file (`ansible-vault`):  
`ansible-vault rekey --vault-id /etc/ansible/auth/auth.secure_passwd --new-vault-id
/etc/ansible/auth/auth.new_secure_passwd a_role_i_made/vars/the_passwd`

* To decrypt the file (`ansible-vault decrypt ...`)
* To create empty encrypted files (`ansible-vault create ...`)

## Notes
* The vault-id label is not always required. 
# And here we have... Ansible

## What is Ansible?
Ansible is an agentless IT automation engine.  Unlike Puppet, Chef, or Salt, 
Ansible can access any machine we access via an SSH connection right now.

Ansible connects to nodes and pushes out small programs, called modules, executes them,
 and removes them when they're finished.

Ansible requires no dedicated servers, daemons, or databases.

Ansible can be run ad-hoc, via playbooks (Ansible scripts), or roles (A combination of Ansible scripts)

Ansible is an Open Source project sponsored by Red Hat - it is free, widely used, and update to date. They 
make their money with Ansible orchestration suites because Ansible is powerful but can become difficult 
to organize at scale.

## Ansible Basics

### Inventory
To start with ansible, we need an inventory.  The default location is `/etc/ansible/hosts`, but can be placed 
where desired.

YAML, INI, and a small variety of other formats are supported.  (`ans_inv_generate.py` outputs an INI 
formatted file.) 

The host file can be as simple as grouped hostnames:
```
mail.example.com

[webservers]
foo.example.com
bar.example.com

[dbservers]
one.example.com
two.example.com
three.example.com
```

There a variety of arguments that can be associated with the hosts files to ensure both an secure shell connection and 
 appropriate user rights. The following are a small sampling of what is available:

| Variable | Purpose |
|---|---|
|`ansible_port`|in case the port isn't 22
|`ansible_host`|if it is different than the alias given
|`ansible_ssh_private_key_file`| the path to ssh private key|
 
 There are also arguments for sudo'ing, extended ssh/sftp/scp variables, and which executables 
 should be involved in the process.
  
Ansible can be run ad-hoc without an inventory files, but playbooks and roles will need an inventory to 
be effective.

### Ad Hoc commands
`ansible ${HOSTNAME/HOSTGROUPNAME} -m ${MODULENAME} -a ${MODULEARGUMENTS}`

|Variable|Purpose|
|---|---|
| ${HOSTNAME/HOSTGROUPNAME} | Either a hostname from the inventory or a host group from the inventory|
| ${MODULENAME} | The module to run|
| ${MODULEARGUMENTS} | The CLI arguments for the chosen module|

For example, the following will connect to any host in the __physical__ host group established in the example hosts
 file and restart apache:

```
ansible physical -m service -a "name=httpd state=restarted"
```

This will check the uptime of all of the __aws__ host group:
```
ansible aws -m shell -a "uptime"
```

This will "gather facts" on __gusta__ - facts represent discovered variables about a system.  They can be used for adding 
variables to playbooks but can also be used as an information gathering tool:
```
ansible gusta -m setup -a "filter='*distribution*'"
``` 

### Playbooks
Ansible playbooks are repeatable ansible module scripts.  They are usually written in YAML but can be in other languages 
like JSON with the use of plugins.  Why YAML?  YAML is inherently human readable, YAML files are pretty-printed by default  
This simple example, poc_yum.yml, unlocks an rpm, updates the rpm, then relocks it.
```yaml
---
- name: Yum proof of concept
  hosts: "{{ inventory_hostname }}"

  tasks:
    - name: YUM version unlock
      shell: /usr/bin/yum versionlock delete "{{ update_rpm }}"
      failed_when: false
      changed_when: false

    - name: YUM update
      yum:
        name: "{{ update_rpm }}"

    - name: YUM version lock
      shell: /usr/bin/yum versionlock "{{ update_rpm }}"
```
It can be run with:
 
```bash
[root@ansible_poc]$ ansible-playbook poc-yum.yml -e "inventory_hostname=m505.awsasp.com update_rpm=*am-mailroom*"
```

* `-e` provides the data for the variables in the playbook.  There are a wide array of options for providing the answers to these
variables.  `-e` was easiest for such a playbook.  `inventory_hostname` in this situation could be a single hostname, a host group, 
or "all"
* `hosts: "{{ inventory_hostname }}"` can be looked at as an example of how variables are included in playbooks with double curly brackets.
* `tasks` is the list of actions the playbook will take.  Under the name of each task is the name of the module
being used.  This is how modules are called.  The indented entries under the module are the arguments.  If not indented, then it is an
ansible statement, not a module specific one. 
* `failed_when: false` and `changed_when: false` are one of the ways to handle errors that can stop the progress of the
playbook.  In this case, the unlock was causing a failure because not every lock file includes every rpm that can be
called.  As opposed to erroring out, those statements allow it to continue processing while keeping any logging data
caused by the failure.

### Roles
Roles are the primary mechanism for breaking a playbook into multiple files.  There is now an example - this project 
holds a directory called `osquery` that will install the osquery package on Redhat and Ubuntu hosts. 
```
/osquery
   - osquery-role.yml
   /rhel-role
      /files
         - RPM-GPG-KEY-osquery
      /tasks
         - main.yml
   /ubuntu-role
      /tasks
         - main.yml
  
```
```
[root@ansible_poc:osquery]$ ansible-playbook osquery.yml -e "inventory_hostname=m505.awsasp.com"
```
The `osquery-role.yml` file contains syntax calling to two roles: rhel-role & ubuntu-role.  Using standard ansible facts - 
the system information that is available for any ansible command to leverage - the initial playbook (osquery-role.yml) reaches 
out to the host and checks the value of the `ansible_distribution` fact.  

If the host reports it is a `Red Hat` derivative, it carries on to 
the `rhel-role` folder.  In there it runs the ansible-playbook under the tasks directory.  That playbook lacks any of the preamble
in the solo playbooks rather it acts as a continuation of the original playbook.  That tasks folder is to the role as the tasks 
section is to a stand-alone playbook.  There is one file that needs to be transferred as part of the playbook if the host is 
RHEL based.  That file exists in 'files' and is copied over using a copy command in the role's main.yml.  The playbook copies the 
gpg-key over, installs the repo, installs osquery with yum, then runs the osqueryi command to verify it works.  

If the host reports it is `Debian` derivative, it carries on to the `ubuntu-role` folder.  In there is one folder, tasks, 
which includes one file that installs the apt repo key, installs the apt repo, installs the package, and then verifies that
 osqueryi works.

Roles can also contain Jinja2 templates, default settings, and variable settings folders.

Roles provide the means to have ansible playbooks clumped together to make a complex and repeatable process for nearly any task.

## Jinja2
Jinja2 is a Python based web templating framework and is used in Ansible built-in filters and templates.  Whether in a playbook or in
a template, ansible uses Jinja2 style expressions for its variables.
* `{% %}` is used for statements in a Jinja2 template.
* `{{ }}` are capable of holding a wide variety of literals, comparisons, logic, math and the ability to call Jinja2 filters.

#### Templates
A template in Ansible is a file which contains configuration parameters that are populated by dynamic values.  Variables
driven by a playbook can be written out to files via Jinja2 templates.  These templates can contain control structures, expressions, and filters.
The following is a template named `atemplate.j2`.  It is called to run a for loop on a list generated by one of the playbooks.  
All it does is write the list in the variable to the file.
```jinja2
{% for item in alist %}
{{ item }}
{% endfor %}
```

The following is an example call to a template in a playbook, 
```yaml
   - name: Write alist to file
     template:
       src: atemplate.j2
       dest: "/tmp/theoutputlist.txt"
```
The module `template` is being given the argument `src`, which is the name of the template, and `dest`, which is the output file's name. 

Jinja2 templates can be used to output a variety of files, including system files like `/etc/sysconfig/iptables`, `/etc/sysconfig/network-scripts/ifcfg-eth0`, or even 
`/amdb/etc/company.xml`.

#### Filters
Filters are variable modifiers.  They are placed inside the double curly braces after the variable and are separated by a pipe.  Filters
that accept arguments have parentheses.  Jinja2 includes a variety of built-in filters, Ansible adds a variety as well.  Here is an example
```jinja2
alist: "{{ avar | json_query("ajson_key") }}"
```
This takes a variable, runs a filter, and assigns it as a new variable.
* `avar` is the var.  In this case, it is a json block.
* `json_query("ajson_key")` takes the json and drills to the the key named `ajson_key`
* `alist` takes the results of the json query and applies it to a variable.  The variable in this example is a list.
It is the list used by the previous Jinja2 template. 
 


## Modules
Much of Ansible's power comes from its modules.  Ansible's website has a lot of documentation for the standard modules, including
easy to read tables of the arguments and multiple examples of each.

These two modules are being used in the sample playbook provided in the 'Playbook' section: `shell` and `yum`
* `shell` uses a `free_form` parameter - the command that follows the module name, with its arguments space delimited, is that
parameter.  `shell` module arguments can be passed with an `args` statement.  `chdir`,`creates`, `removes`, and `executable` are 
all examples of passable module arguments.  Because of the `free_form` parameter, no argument parametes are essential 
for the module to execute.
* `yum` does not have a `free_form` parameter, the parameters given are necessary for the `yum` module to work.  The module assumes the
intent is to install a yum module, so passing the `name` parameter tells the module to install the named package.

Ansible modules can be found:
 * on [Ansible's documentation site](https://docs.ansible.com/ansible/latest).
 * with the `ansible-doc` command and the `-l` argument.
 
 
## Callbacks
Ansible returns information during every run.  This data output is called a callback.  Ansible's callbacks can be changed in /etc/ansible/ansible.cfg.
There are some of that effect stdout and some that do other work behind the scenes, like sending emails or updating 
databases.  Only one stdout callback can be enabled at a time. 
There are others that can be stacked that do not need to monopolize stdout and therefore can be run at the same time.
Examples of some default callbacks are:
* rocketchat - rocketchat integration
* mail - mail errors to smtp address
* timer - appends the playbook run time to stdout
* grafana-notations - outputs grafana annotations over http api

Custom callbacks can be written as needed.

```yaml
- name: ${{ Descriptive rocket.chat integration message.}}
  rocketchat:
    token: ${{_THE_WEBHOOK_TOKEN_FROM_ROCKETCHAT-DO_NOT_INCLUDE_PRECEEDING_SLASH_}} 
    domain: chat.yourdomain.org
    msg: ${{ Message to be shown in rocket.chat }}
  delegate_to: localhost
```


## Notes
* adding `become: yes` to a command is a default method to tell ansible to sudo to root when running that command.
* enabling `pipelining` on the ansible server makes a ***HUGE*** difference in execution speed.
* `register` is used to register variables for later use.
* 
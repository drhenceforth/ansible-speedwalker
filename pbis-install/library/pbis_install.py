#!/usr/bin/python

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'drhenceforth'
}

DOCUMENTATION = '''
---
module: pbis_install

short_description: A module to assist with PBIS installations

version_added: "2.8"

description:
    - "Runs pbis shell commands with idempotence"

options:
    install:
        description:
            - Use the module to install PBIS
              True = install
              False = do not install (default)
        required: false
    domainjoin:
        description:
            - Use the module to join the host to a domain
              True = perform the domain join
              False = do not domain join (default)
        required: false
    super_username:
        description:
            - Name of the super user that can join the machine to the domain
        required: false
    super_password:
        description:
            - Password for the domain joining superuser
        required: false
    ou:
        description:
            - The organizational unit the system belongs in
        required: false
    domain: 
        description:
            - The domain being joined
        required: true
    installer:
        description:
            - The name of the PBIS install script to run
        required: false 

author:
    - DrHenceforth (richardwinne@gmail.com)
'''

EXAMPLES = '''
# Install PBIS on the machine
- name: Install PBIS
  pbis_install:
    domain: yourdomain.org
    install: True

# Domain Join
- name: Join the machine to the domain 
  pbis_install:
    domainjoin: True
    super_username: pbisadjoin
    super_password: {{ vault_pbisadd }}
    ou: "Computers"
    domain: yourdomain.org
    
# Set default user shell
- name: Set the default user shell
  pbis_install:
    domain: yourdomain.org
    set_shell: '/bin/zsh'
'''

RETURN = '''
message:
    description: The stdout from the PBIS open shell command.
'''

from ansible.module_utils.basic import AnsibleModule
import subprocess
import os
import re
import sys

# This script strips white space and blank lines with line comprehensions from all subprocess output
# so it will present cleanly in the Ansible stdout callbacks.

# Using the AnsibleModule function to create the module arguments - creates a separate dictionary 
# for each argument and stores all of those dictionaries in the module_args dictionary.
module_args = dict(
    install=dict(type='bool', required=False, default=False),
    domainjoin=dict(type='bool', required=False, default=False),
    super_username=dict(type='str', required=False, default='pbisadjoin'),
    super_password=dict(type='str', required=False, default='noneya', no_log=True),
    ou=dict(type='str', required=False, default='Computers'),
    domain=dict(type='str', required=True),
    installer=dict(type='str', required=False, default='pbis-open-8.8.0.506.linux.x86_64.rpm.sh'),
    installer_path=dict(type='str', required=False, default='/tmp/pbis-installer/'),
    set_shell=dict(type='str', required=False, default="False"),
)

# Creates the module object from the dictionary of dictionaries.
module = AnsibleModule(
    argument_spec=module_args
)


def run_module():
    """
    Everything is routed from here based on what the intention is: install it, domain join it,
    or set the default shell on it.
    """
    if module.params['install'] == True:
        installscript = module.params['installer_path'] + module.params['installer']
        installpbis(installscript)
    elif module.params['domainjoin'] == True:
        domain_join_pbis(module.params['super_username'], module.params['super_password'], module.params['ou'],
                         module.params['domain'])
    elif module.params['set_shell'] != 'False':
        set_default_shell(module.params['set_shell'])


def installpbis(installcmd):
    """
    Checks for the pbis utility binary.  If it exists, it runs the status utility grabs the output and 
    exits.  If it doesn't exist, it runs the pbis installer, grabs the output and exits.  If it fails,
    it outputs the Exception and exits with the fail_json function.  Now supporting OS X!!!
    :param installcmd: The full path to where the installer has been copied.
    :return: exits with AnsibleModule functions: exit_json and fail_json
    """
    exists = os.path.isfile('/opt/pbis/bin/lsa')
    if exists:
        install_change_status = False  # because it exists, nothing is changing
        install_output_message = subprocess.Popen('/opt/pbis/bin/pbis-status', stdout=subprocess.PIPE,
                                                  stderr=subprocess.STDOUT)
        install_output_message = [i.strip() for i in install_output_message.stdout.readlines() if i != '\n']
    else:
        if sys.platform == 'darwin':
            try:
                # For Mac OS X, mount the .dmg, open the volume on the mac, then run the package in unmanned mode
                subprocess.call(['hdiutil', 'mount', installcmd])
                subprocess.call(['/usr/bin/open', '/Volumes/pbis-open'])
                install_output_message = subprocess.Popen(
                    ['installer', '-pkg', '/Volumes/pbis-open/pbis-open-8.8.0.506.pkg', '-target', 'LocalSystem'],
                    stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
                install_output_message = [i.strip() for i in install_output_message.stdout.readlines() if i != '\n']
                if os.path.exists('/Volumes/pbis-open'):
                    subprocess.call(['hdiutil', 'unmount', '/Volumes/pbis-open'])
                install_change_status = True
            except Exception as e:
                module.fail_json(msg='The OS X Failed!!!! PBIS install failed due to {0}'.format(e))
        else:
            try:
                install_change_status = True
                install_output_message = subprocess.Popen(installcmd, stdout=subprocess.PIPE,
                                                          stderr=subprocess.STDOUT)
                install_output_message = [i.strip() for i in install_output_message.stdout.readlines() if i != '\n']
            except Exception as e:
                module.fail_json(msg='AHHHH!!! Failed!!!! PBIS install failed due to {0}'.format(e))

    results = dict(
        changed=install_change_status,
        message=install_output_message
    )
    module.exit_json(**results)


def domain_join_pbis(joinuser, joinpwd, joinou, joindomain):
    """
    Uses pbis domainjoin to add the host to the domain.
    :param joinuser: The super user empowered to make a domain add.
    :param joinpwd: The super user's password, passed from an ansible vault file.
    :param joinou: The ou the host should be dropped into.
    :param joindomain: The domain the host should be added to.
    :return: exits with AnsibleModule functions: exit_json and fail_json
    """
    joiner_binary_check = os.path.isfile('/opt/pbis/bin/domainjoin-cli')
    if not joiner_binary_check:
        module.fail_json(msg='/opt/pbis/bin/domainjoin-cli does not exist')
    joined_check = subprocess.Popen(['/opt/pbis/bin/domainjoin-cli', 'query'], stdout=subprocess.PIPE,
                                    stderr=subprocess.STDOUT)
    # joined_check = joined_check.stdout.readlines()
    joined_check = [i.strip() for i in joined_check.stdout.readlines() if
                    i != '\n']  # strip white space and blank lines
    domained = False
    for i in joined_check:
        if re.search(joindomain.upper(), i):
            domained = True
    if domained:
        domain_change_status = False  # because it exists, nothing is changing
        domain_output_message = joined_check
    else:
        try:
            domain_output_message = subprocess.Popen(
                ["/opt/pbis/bin/domainjoin-cli", "join", "--ou", joinou, joindomain, joinuser, joinpwd],
                stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            domain_output_message = [i.strip() for i in domain_output_message.stdout.readlines() if i != '\n']
            domain_change_status = True
        except Exception as e:
            module.fail_json(msg='NOOOO!!! Failed!!!! PBIS failed to join the domain because: {0}'.format(e))

    results = dict(
        changed=domain_change_status,
        message=domain_output_message
    )
    module.exit_json(**results)


def set_default_shell(shell_path):
    """
    Makes sure the PBIS config utility exists.  If it does, it checks the currently configured shell.  
    If it matches, it grabs stdout, and exits.  If it doesn't, it runs the command, grabs stdout, and 
    exits.  If it errors, it exits with the fail_json function.
    :param shell_path: the full path of the shell binary to set as default.
    :return: exits with AnsibleModule functions: exit_json and fail_json
    """
    pbis_con_bin_chk = os.path.isfile('/opt/pbis/bin/config')
    if not pbis_con_bin_chk:
        module.fail_json(msg='/opt/pbis/bin/config does not exist')

    shell_check = subprocess.Popen(["/opt/pbis/bin/config", "--show", "LoginShellTemplate"], stdout=subprocess.PIPE,
                                   stderr=subprocess.STDOUT)
    shell_check = [i.strip() for i in shell_check.stdout.readlines() if i != '\n']  # strip white space and blank lines
    shelled = False
    for i in shell_check:
        if re.search(shell_path, i):
            shelled = True  # yes, the shell exists, this system is already configured with the desired shell.
    if shelled:
        shell_change_status = False  # because it exists, nothing is changing
        shell_output_message = shell_check
    else:
        try:
            shell_output_message = subprocess.Popen(["/opt/pbis/bin/config", "LoginShellTemplate", shell_path])
            shell_output_message = [i.strip() for i in shell_output_message.stdout.readlines() if i != '\n']
            shell_change_status = True  # because it was just installed, it changed the system, so 'True'
        except Exception as e:
            module.fail_json(msg='Failed to set the default shell because: {0}'.format(e))

    results = dict(
        changed=shell_change_status,
        message=shell_output_message
    )

    module.exit_json(**results)


if __name__ == '__main__':
    run_module()
